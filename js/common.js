jQuery( document ).ready(function( $ ) {
	
	$("[data-trigger]").on("click", function(){
		var trigger_id =  $(this).attr('data-trigger');
		$(trigger_id).toggleClass("show");
		$('body').toggleClass("offcanvas-active");
	});

	// close button 
	$(".btn-close").click(function(e){
		$(".navbar-collapse").removeClass("show");
		$("body").removeClass("offcanvas-active");
	}); 
	
	$("a.mastheadbutton").hover(function() {
		$("a.mastheadbutton i").stop().animate({
			top: 11,
			opacity: 1
		}, 300);
	}, function() {
		$("a.mastheadbutton i").stop().animate({
			top: 0,
			opacity: 0
		}, 300);
	});
	
	$("a.contentbutton").hover(function() {
		$("a.contentbutton i").stop().animate({
			right: 40,
			opacity: 1
		}, 300);
	}, function() {
		$("a.contentbutton i").stop().animate({
			right: 70,
			opacity: 0
		}, 300);
	});
	
	$("a.blankcontentbutton").hover(function() {
		$("a.blankcontentbutton i").stop().animate({
			right: 40,
			opacity: 1
		}, 300);
	}, function() {
		$("a.blankcontentbutton i").stop().animate({
			right: 70,
			opacity: 0
		}, 300);
	});
		
	$("#mastheadgames").not(".slick-initialized").slick({
		slidesToShow: 7,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		adaptiveHeight: false,
		dots: false,
		arrows: false,
		centerMode: false,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});
	
	$("#appgames").not(".slick-initialized").slick({
		slidesToShow: 7,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		adaptiveHeight: false,
		dots: false,
		arrows: false,
		centerMode: false,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});
	
	$(document).ready(function() {
        $('html, body').hide();
        if (window.location.hash) {
            setTimeout(function() {
                $('html, body').scrollTop(0).show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top - 90
                }, 500);
				var uri = window.location.toString();
				if (uri.indexOf("#") > 0) {
					var clean_uri = uri.substring(0, uri.indexOf("#"));
					window.history.replaceState({}, document.title, clean_uri);
				}
            }, 0);
        } else {
            $('html, body').show();
        }
    });
	
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top - 90
				}, 500);
				return false;
			}
		}
	});
	
});